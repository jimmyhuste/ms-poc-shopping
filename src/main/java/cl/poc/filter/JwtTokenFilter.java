package cl.poc.filter;

import cl.poc.util.JwtTokenUtil;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.ArrayList;

;

@Component
@RequiredArgsConstructor
public class JwtTokenFilter extends OncePerRequestFilter {
	
	private final JwtTokenUtil jwtTokenUtil;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
		// Get authorization header and validate
		final String authorization = request.getHeader(HttpHeaders.AUTHORIZATION);
		
		if (ObjectUtils.isEmpty(authorization) || !authorization.toUpperCase().startsWith("BEARER ")) {
			chain.doFilter(request, response);
			return;
		}
		
		// Get jwt token and validate
		final String token = authorization.split(" ")[1].trim();
		
		final String rutToken = jwtTokenUtil.getClaimFromToken(token, "rut_cliente");
		final String roles = jwtTokenUtil.getClaimFromToken(token, "roles");
		
		if (!jwtTokenUtil.isValidToken(token)) {
			chain.doFilter(request, response);
			return;
		}
		
		
		UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken("userDetails", null, new ArrayList<>());
		
		usernamePasswordAuthenticationToken.setDetails(request);
		
		SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
		chain.doFilter(request, response);
		
		// new WebAuthenticationDetailsSource().buildDetails(request)
	}
}
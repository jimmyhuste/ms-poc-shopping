package cl.poc.exception;


import cl.poc.dto.error.ErrorDTO;
import cl.poc.dto.error.ErrorDetailDTO;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase de definicion ExceptionManager, que maneja excepciones centralizadas
 *
 * @author Jimmy Huste
 */
@ControllerAdvice
@Order(Ordered.LOWEST_PRECEDENCE)
@Slf4j
public class ExceptionManager {
	
	/**
	 * Manejo de excepciones no consideradas en ningun otro manejador.
	 * Para ellas se responde un HttpStatus#INTERNAL_SERVER_ERROR.
	 *
	 * @param exception se inyectara cualquier excepcion tecnica que no haya sido controlada
	 * @return error con HttpStatus 500 y el mensaje de error respectivo
	 */
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	@Order(Ordered.LOWEST_PRECEDENCE)
	public ResponseEntity<ErrorDTO> manageInternalServerErrorByDefault(Exception exception) {
		log.error("", exception);
		ErrorDTO errorDto = ErrorDTO
				.builder()
				.timestamp(LocalDateTime.now().toString())
				.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
				.error(exception.getMessage())
				.path("path")
				.build();
		return new ResponseEntity<>(errorDto, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
//	 * Metodo encargado de tratar excepcion MethodArgumentNotValidException. <br>
//	 * Constantes#COD_VALIDACION_PARAMETROS_NO_VALIDOS y mensaje
//	 * Constantes#MSG_VALIDACION_PARAMETROS_NO_VALIDOS <br>
//	 *
//	 * @param excepcion MethodArgumentNotValidException atributo del tipo MethodArgumentNotValidException usado para
//	 * excepciones de validación (Utiliazadas en javax.validation.constraints y org.hibernate.validator.constraints)
//	 * @return ResponseEntity&lt;Object&gt; Incluye el detalle de parámetros de entrada y validaciones no exitosas
//	 * @author Jimmy Huste
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ErrorDTO> manageMethodArgumentNotValidException(MethodArgumentNotValidException exception) {
		ErrorDTO errorDto = ErrorDTO
				.builder()
				.timestamp(LocalDateTime.now().toString())
				.status(HttpStatus.BAD_REQUEST.value())
				.error(exception.getMessage())
				.path("path")
				.details(obtenerErrores(exception))
				.build();
		
		log.error("", exception);
		return new ResponseEntity<>(errorDto, new HttpHeaders(), HttpStatus.BAD_REQUEST);
	}

	private List<ErrorDetailDTO> obtenerErrores(MethodArgumentNotValidException e) {
		List<ErrorDetailDTO> listaErroresValidacion = new ArrayList<>();
		List<FieldError> errors = e.getBindingResult().getFieldErrors();
		for (FieldError error : errors) {
			listaErroresValidacion.add(ErrorDetailDTO
							.builder()
								.field(error.getField())
								.message(error.getDefaultMessage())
							.build());
		}
		return listaErroresValidacion;
	}
	
	@ExceptionHandler(ResponseStatusException.class)
	public ResponseEntity<ErrorDTO> manageMethodArgumentNotValidException(ResponseStatusException exception, HttpServletRequest request) {
		ErrorDTO errorDto = ErrorDTO
				.builder()
				.timestamp(LocalDateTime.now().toString())
				.status(exception.getStatusCode().value())
				.error(exception.getReason())
				.path(request.getRequestURI().substring(request.getContextPath().length()))
				//.path(request.getRequestURL().toString().split("ms-poc")[1])
				.build();
		
		log.error("", exception);
		return new ResponseEntity<>(errorDto, new HttpHeaders(), exception.getStatusCode().value());
	}
}

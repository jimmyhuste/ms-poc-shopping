package cl.poc.model.product;

import jakarta.persistence.*;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Builder
@Entity(name = "categoria")
@Table
public class Category {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_categoria", nullable = false)
	private Long id;
	@Column(name = "nombre", nullable = false)
	private String nombre;
	@Column(name = "descripcion")
	private String descripcion;
}

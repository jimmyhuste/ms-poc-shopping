package cl.poc.model.product;

import jakarta.persistence.*;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Builder
@Entity(name = "fotografia")
@Table
public class Fotografia {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "nombre", nullable = false)
	private String nombre;
	@Column(name = "foto", nullable = false)
	private byte[] foto;
	@Column(name = "extension", nullable = false )
	private String extension;
	
	@Column(name = "id_producto", nullable = true, insertable = true, updatable = true)
	private Long idProducto;

}

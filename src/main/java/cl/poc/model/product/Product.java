package cl.poc.model.product;



import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Builder
@Entity(name = "producto")
@Table
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "nombre", nullable = false)
	private String nombre;
	@Column(name = "precio", nullable = false )
	private Double precio;
	@Column(name = "descuento")
	private String descuento;
	@Column(name = "descripcion")
	private String descripcion;
	@Column(name = "id_categoria", nullable = false)
	private Long idCategoria;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "id_categoria", nullable = false, insertable = false, updatable = false)
	private Category category;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_producto", nullable = false, insertable = false, updatable = false)
	private List<Fotografia> fotografias;

}

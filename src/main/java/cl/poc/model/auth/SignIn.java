package cl.poc.model.auth;

import jakarta.persistence.*;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Builder
@Entity(name = "login")
@Table
public class SignIn {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_usuario", nullable = false)
	private Long id;
	@Column(name = "usuario", nullable = false)
	private String username;
	@Column(name = "password", nullable = false )
	private String password;
}

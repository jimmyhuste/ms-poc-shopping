package cl.poc.model.auth;

import jakarta.persistence.*;
import lombok.*;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Builder
@Entity(name = "usuario")
@Table
public class SignUp {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "rut", nullable = false)
	private String rut;
	@Column(name = "nombre", nullable = false)
	private String nombre;
	@Column(name = "apellidoMaterno", nullable = false )
	private String apellidoMaterno;
	@Column(name = "apellidoPaterno" , nullable = false)
	private String apellidoPaterno;
	@Column(name = "telefono", nullable = false)
	private String telefono;
	@Column(name = "email" , nullable = false)
	private String email;
	
	@Column(name = "direccion")
	private String direccion;
	
	@Column(name = "foto")
	private String foto;
	@Column(name = "fechaNacimiento")
	private String fechaNacimiento;
	@Column(name = "sexo")
	private String sexo;
	
	@Column(name = "id_usuario")
	private Long idUsuario;
	
	@OneToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "id_usuario", nullable = false, insertable = false, updatable = false)
	private SignIn signIn;
	
}

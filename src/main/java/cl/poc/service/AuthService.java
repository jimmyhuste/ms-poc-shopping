package cl.poc.service;

import cl.poc.dto.auth.SignInDTO;
import cl.poc.dto.auth.SignInReponseDTO;
import cl.poc.dto.auth.SignUpDTO;
import cl.poc.model.auth.SignIn;
import cl.poc.model.auth.SignUp;
import cl.poc.repository.SignInRepository;
import cl.poc.repository.SignUpRepository;
import cl.poc.util.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthService {
	final SignUpRepository signUpRepository;
	final SignInRepository signInRepository;
	
	final JwtTokenUtil jwtTokenUtil;

	public void signup(SignUpDTO signUpDTO) {
		if (signUpRepository.existsByRut(signUpDTO.getRut()) || signInRepository.existsByUsername(signUpDTO.getUser())){
			throw new ResponseStatusException(HttpStatusCode.valueOf(400), "Usuario ya se encuentra registrado.");
		}
		
			signInRepository.save(SignIn
					.builder()
							.username(signUpDTO.getUser())
							.password(signUpDTO.getPassword())
					.build());
		
		SignIn signInId = signInRepository.findByUsername(signUpDTO.getUser()).get();
			signUpRepository.save(SignUp
					.builder()
							.rut(signUpDTO.getRut())
							.nombre(signUpDTO.getNombre())
							.apellidoPaterno(signUpDTO.getApellidoPaterno())
							.apellidoMaterno(signUpDTO.getApellidoMaterno())
							.telefono(signUpDTO.getTelefono())
							.email(signUpDTO.getEmail())
							.fechaNacimiento(signUpDTO.getFechaNacimiento())
							.sexo(signUpDTO.getSexo())
							.direccion(signUpDTO.getDireccion())
							.foto(Base64.getEncoder().encodeToString(signUpDTO.getFoto().getBytes()))
							.idUsuario(signInId.getId())
					.build());
	}
	public SignInReponseDTO login(SignInDTO signInDTO) {
		Optional<SignIn> optionalSignIn = signInRepository.findByUsernameAndPassword(signInDTO.getUser(), signInDTO.getPassword());
		
		if (optionalSignIn.isEmpty()) {
			throw new ResponseStatusException(HttpStatusCode.valueOf(400), "Usuario o Password incorrecto");
		}
		
		SignIn  signIn = optionalSignIn.get();
		SignUp signUp = signUpRepository.findByIdUsuario(signIn.getId());
		
		final List<String> roles = Arrays.asList("PUBLIC", "ADMIN");
		final String token = jwtTokenUtil.generateToken(signUp.getRut(), signIn.getUsername(), roles);
		final int expiration = jwtTokenUtil.getExpirationFromToken(token);
		final String scope = jwtTokenUtil.getScopeFromToken(token);
	return SignInReponseDTO
			.builder()
			.token(token)
			.expiredDate(String.valueOf(expiration))
			.scope(scope)
			.build();
	}
	
	public List<SignUpDTO> findAll() {
		return signUpRepository.findAll()
				.stream().map(user -> SignUpDTO
						.builder()
						.id(user.getId())
						.rut(user.getRut())
						.nombre(user.getNombre())
						.apellidoMaterno(user.getApellidoMaterno())
						.apellidoPaterno(user.getApellidoPaterno())
						.telefono(user.getTelefono())
						.email(user.getEmail())
						.fechaNacimiento(user.getFechaNacimiento())
						.sexo(user.getSexo())
						.direccion(user.getDireccion())
						.foto(Base64.getEncoder().encodeToString(user.getFoto().getBytes()))
						.user(user.getSignIn().getUsername())
						.build())
				.collect(Collectors.toList());
	}
}

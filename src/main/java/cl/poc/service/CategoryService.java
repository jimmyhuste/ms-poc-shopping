package cl.poc.service;

import cl.poc.dto.product.CategoriaDTO;
import cl.poc.model.product.Category;
import cl.poc.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class CategoryService {
	final CategoryRepository categoryRepository;
	public void createCategory(CategoriaDTO categoriaDTO) {
		validarExistenciaCategoria(categoriaDTO.getNombre());
		categoryRepository.save(Category
				.builder()
				.nombre(categoriaDTO.getNombre())
				.descripcion(categoriaDTO.getDescripcion())
				.build());
	}
	
	public void validarExistenciaCategoria(String nombre) {
		Optional<Category> optionalCategory = categoryRepository.findByNombreContainingIgnoreCase(nombre);
		
		if (optionalCategory.isPresent()){
			throw new ResponseStatusException(HttpStatusCode.valueOf(400), "Nombre cateria ya existe.");
			
		}
	}
	
	public Category obtenerCategoria(String nombre) {
		Optional<Category> optionalCategory = categoryRepository.findByNombreContainingIgnoreCase(nombre);
		
		if (optionalCategory.isEmpty()){
			
			throw new ResponseStatusException(HttpStatusCode.valueOf(400), "Nombre cateria no existe.");
		}
		return optionalCategory.get();
	}
}

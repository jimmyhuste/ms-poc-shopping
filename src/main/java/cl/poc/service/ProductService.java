package cl.poc.service;

import cl.poc.dto.archivo.FotografiaDTO;
import cl.poc.dto.product.CategoriaDTO;
import cl.poc.dto.product.ProductDTO;
import cl.poc.model.product.Category;
import cl.poc.model.product.Fotografia;
import cl.poc.model.product.Product;
import cl.poc.repository.FotografiaRepository;
import cl.poc.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductService {
	final ProductRepository productRepository;
	final FotografiaRepository fotografiaRepository;
	final CategoryService categoryService;
	
	public void create(ProductDTO productDTO) {
		
		Category category = categoryService.obtenerCategoria(productDTO.getCategoria().getNombre());
		Product product = Product
				.builder()
				.category(category)
				.idCategoria(category.getId())
				.nombre(productDTO.getNombre())
				.precio(productDTO.getPrecio())
				.descuento(productDTO.getDescuento())
				.descripcion(productDTO.getDescripcion())
				.fotografias(new ArrayList<>())
				.build();
		
		productRepository.save(product);
		
		List<Fotografia> fotografias = productDTO.getFotografias()
				.stream()
				.map( f-> Fotografia
						.builder()
						.id(f.getId())
						.idProducto(product.getId())
						.nombre(f.getNombre())
						.foto(Base64.getDecoder().decode(f.getFoto()))
						.extension(f.getExtension())
						.build())
				.collect(Collectors.toList());
		
		fotografiaRepository.saveAll(fotografias);
		
	}
	
	public List<ProductDTO> findAll() {
		
		return productRepository
				.findAll()
				.stream()
				.map(p-> ProductDTO
						.builder()
						.id(p.getId())
						.nombre(p.getNombre())
						.precio(p.getPrecio())
						.descuento(p.getDescuento())
						.descripcion(p.getDescripcion())
						.fotografias(p.getFotografias()
								.stream()
								.map(f -> FotografiaDTO
								.builder()
										.id(f.getId())
										.nombre(f.getNombre())
										.extension(f.getExtension())
										.foto(Base64.getEncoder().encodeToString(f.getFoto()))
								.build())
								.collect(Collectors.toList()))
						.categoria(CategoriaDTO
								.builder()
								.id(p.getCategory().getId())
								.nombre(p.getCategory().getNombre())
								.descripcion(p.getCategory().getDescripcion())
								.build())
						.build())
				.collect(Collectors.toList());
	}
	
	public Optional<Product> obtenerProducto(Long id) {
		Optional<Product> optionalProduct = productRepository.findById(id);
		
		if (!optionalProduct.isPresent()){
			
			throw new ResponseStatusException(HttpStatusCode.valueOf(400), "Producto no existe.");
		}
		return optionalProduct;
		
	}
	
	public void update(ProductDTO productDTO, Long id) {
		
		Optional<Product> productOptional = obtenerProducto(id);
		
		 if (productOptional.isEmpty()){
			throw new ResponseStatusException(HttpStatusCode.valueOf(400), "Error al actualizar producto");
		}
		
		List<Fotografia> fotografias = productDTO.getFotografias()
				.stream()
				.map( f-> Fotografia
				.builder()
						.id(f.getId())
						.idProducto(productDTO.getId())
						.nombre(f.getNombre())
						.foto(Base64.getDecoder().decode(f.getFoto()))
						.extension(f.getExtension())
						.build())
				.collect(Collectors.toList());
				
		Product product = productOptional.get();
		product.setCategory(categoryService.obtenerCategoria(productDTO.getCategoria().getNombre()));
		product.setNombre(productDTO.getNombre());
		product.setPrecio(productDTO.getPrecio());
		product.setDescuento(productDTO.getDescuento());
		product.setDescripcion(productDTO.getDescripcion());
		product.setFotografias(fotografias);
		productRepository.save(product);
		
	}
	
	public ProductDTO getOne(Long id) {
		Optional<Product> productOptional = productRepository.findById(id);
		
		if (!productOptional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Producto no existe.");
		}
		Product product = productOptional.get();
		List<FotografiaDTO> fotografias = product
				.getFotografias()
				.stream()
				.map(f -> FotografiaDTO
				.builder()
						.id(f.getId())
						.nombre(f.getNombre())
						.foto(Base64.getEncoder().encodeToString(f.getFoto()))
				.build())
				.collect(Collectors.toList());
		
		return ProductDTO.builder()
				.id(product.getId())
				.nombre(product.getNombre())
				.precio(product.getPrecio())
				.descuento(product.getDescuento())
				.descripcion(product.getDescripcion())
				.fotografias(fotografias)
				.categoria(CategoriaDTO.builder()
						.id(productOptional.get().getCategory().getId())
						.nombre(productOptional.get().getCategory().getNombre())
						.descripcion(productOptional.get().getCategory().getDescripcion())
						.build())
				.build();
	}
	
	public void delete(Long id) {
		Optional<Product> productOptional = productRepository.findById(id);
		
		if (!productOptional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Producto no existe.");
		}
		productRepository.delete(productOptional.get());
	}
}

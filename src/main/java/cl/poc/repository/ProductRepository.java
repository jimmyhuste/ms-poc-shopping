package cl.poc.repository;


import cl.poc.model.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
//	Product findByIdProduct(Long idProducto);

}

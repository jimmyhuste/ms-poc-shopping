package cl.poc.repository;

import cl.poc.model.product.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository

public interface CategoryRepository extends JpaRepository<Category, Long> {
	Optional<Category> findByNombreContainingIgnoreCase(String nombre);

}

package cl.poc.repository;

import cl.poc.model.auth.SignIn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SignInRepository extends JpaRepository<SignIn, Long> {
	Optional<SignIn> findByUsername(String username);
	
	Boolean existsByUsername(String username);
	
	Optional<SignIn> findByUsernameAndPassword(String username, String password);
}

package cl.poc.repository;

import cl.poc.model.auth.SignUp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SignUpRepository extends JpaRepository<SignUp, Long> {
	Boolean existsByRut(String rut);
	SignUp findByIdUsuario(Long idUsuario);
}

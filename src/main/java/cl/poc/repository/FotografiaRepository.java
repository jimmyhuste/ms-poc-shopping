package cl.poc.repository;

import cl.poc.model.product.Fotografia;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FotografiaRepository extends JpaRepository<Fotografia, Long> {


}

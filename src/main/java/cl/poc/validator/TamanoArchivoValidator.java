package cl.poc.validator;

import cl.poc.constraint.TamanoArchivo;
import cl.poc.util.ArchivoUtil;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class TamanoArchivoValidator implements ConstraintValidator<TamanoArchivo, Object> {
	
	private int min;
	private int max;
	
	@Override
	public void initialize(TamanoArchivo parameters) {
		min = parameters.min();
		max = parameters.max();
		validateParameters();
	}
	
	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		long tamano = ArchivoUtil.calcularTamanoDeArchivoBase64EnKilobytes(String.valueOf(value));
		return tamano >= min && tamano <= max;
	}
	
	private void validateParameters() {
		if (min < 0) {
			throw new IllegalArgumentException("HV000264: El parametro min no puede ser negativo");
		}
		if (max < 0) {
			throw new IllegalArgumentException("HV000265: El parametro max no puede ser negativo");
		}
		if (max < min) {
			throw new IllegalArgumentException("HV000266: El parametro max no puede ser menor al parametro min");
		}
	}
}
package cl.poc.validator;

import cl.poc.constraint.MimeType;
import cl.poc.util.ArchivoUtil;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.Arrays;
import java.util.stream.Collectors;

public class MimeTypeValidator implements ConstraintValidator<MimeType, Object> {
	
	private String[] extensions;
	
	@Override
	public void initialize(MimeType parameters) {
		extensions = parameters.extensions();
		validateParameters();
	}
	
	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		return Arrays
				.stream(extensions)
				.collect(Collectors.toList())
				.contains(ArchivoUtil.obtenerExtensionDeArchivoBase64(String.valueOf(value)));
	}
	
	private void validateParameters() {
		if (extensions.length == 0) {
			throw new IllegalArgumentException("HV000267: El parametro extensions no puede venir vacio");
		}
	}
}
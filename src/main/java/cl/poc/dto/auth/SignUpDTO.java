package cl.poc.dto.auth;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SignUpDTO {
	
	private Long id;
	
	@Valid
	@NotNull(message = "Rut es obligatorio")
	private String rut;
	
	@Valid
	@NotNull(message = "Nombre es obligatorio")
	@Pattern(regexp = "[A-Za-zÁÉÍÓÚñáéíóúÑ ]+", message = "El formato de los nombres debe ser Aaaa")
	@Size(max = 25, message = "Los nombres no pueden superar el maximo de 25 caracteres")
	private String nombre;
	
	@Valid
	@NotNull(message = "Appelido Materno es obligatorio")
	@Pattern(regexp = "[A-Za-zÁÉÍÓÚñáéíóúÑ ]+", message = "El formato del apellido materno debe ser Dddd")
	@Size(max = 25, message = "El apellido materno no puede superar el maximo de 25 caracteres")
	private String apellidoMaterno;
	
	@Valid
	@NotNull(message = "Apellido Paterno es obligatorio")
	@Pattern(regexp = "[A-Za-zÁÉÍÓÚñáéíóúÑ ]+", message = "El formato del apellido paterno debe ser Cccc")
	@Size(max = 25, message = "El apellido paterno no puede superar el maximo de 25 caracteres")
	private String apellidoPaterno;
	
	@Valid
	@Pattern(regexp = "\\d+", message = "El formato del telefono debe ser numerico")
	@Size(max = 9, message = "El telefono debe poseer un largo maximo de 9 caracteres")
	private String telefono;
	
	@Valid
	@Size(max = 60, message = "El email no puede superar el maximo de 60 caracteres")
	private String email;
	
	@Valid
	@Pattern(regexp = "^(19[0-9]{2}|2[0-9]{3})-(0[1-9]|1[012])-([123]0|[012][1-9]|31)$", message = "El formato de la fecha de nacimiento debe ser yyyy-MM-dd")
	private String fechaNacimiento;
	
	@Valid
	@Size(min = 1, message = "El sexo debe poseer un largo maximo de 1 caracter")
	@Pattern(regexp = "[MF]", message = "El formato del sexo solo admite M o F")
	private String sexo;
	@Valid
	@Size(max = 60, message = "La direccion no puede superar el maximo de 60 caracteres")
	private String direccion;
	
	@Valid
	private String foto;
	
	@Valid
	@NotNull(message = "Usuario es obligatorio")
	@Size(min = 4, max = 50, message = "La direccion no puede superar el maximo de 60 caracteres")
	private String user;
	
	@Valid
	@NotNull(message = "Password es obligatorio")
	@Size(min = 8, max = 50, message = "El password no puede superar el maximo de 60 caracteres")
	private String password;
	
	
}

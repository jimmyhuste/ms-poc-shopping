package cl.poc.dto.product;

import cl.poc.dto.archivo.FotografiaDTO;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductDTO {
	
	private Long id;
	
	@Pattern(regexp = "[A-Za-zÁÉÍÓÚñáéíóúÑ ]+", message = "El formato de los nombres debe ser Aaaa")
	@Size(max = 50, message = "Los nombres no pueden superar el maximo de 25 caracteres")
	@NotNull(message = "El nombre no puede ir vacío")
	private String nombre;
	
	@Max(value = 10000)
	@NotNull(message = "La precio no puede ir vacío")
	private Double precio;
	
	@NotNull(message = "La categoria no puede ir vacío")
	@Valid
	private CategoriaDTO categoria;
	private String descuento;
	
	@NotNull(message = "Las fotografias no puede ir vacío")
	@Valid
	private List<FotografiaDTO> fotografias;
	
	private String descripcion;
}

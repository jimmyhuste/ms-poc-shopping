package cl.poc.dto.archivo;

import cl.poc.constraint.MimeType;
import cl.poc.constraint.TamanoArchivo;
import jakarta.validation.constraints.NotBlank;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FotografiaDTO {
	private Long id;
	private String nombre;
	
	@NotBlank(message = "El archivo es obligatorio")
	@MimeType(extensions = {"image/jpeg", "image/png", "image/gif", "image/jpg"}, message = "El mime type del archivo debe ser image/jpeg, image/png, image/gif, image/jpg")
	@TamanoArchivo(max = 4000, message = "El tamano de la no puede superar los 4000 kilobytes") // 4000 KB = 4 MB
	@ToString.Exclude
	private String foto;
	
	private String extension;
}

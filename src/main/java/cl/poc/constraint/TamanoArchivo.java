package cl.poc.constraint;

import cl.poc.validator.TamanoArchivoValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Target( { ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = TamanoArchivoValidator.class)
public @interface TamanoArchivo {
	
	String message() default "";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
	int min() default 0;
	int max() default Integer.MAX_VALUE;
	
}

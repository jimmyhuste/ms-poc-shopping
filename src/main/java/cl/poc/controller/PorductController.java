package cl.poc.controller;

import cl.poc.dto.product.ProductDTO;
import cl.poc.service.ProductService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("products")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class PorductController {
	private final ProductService productService;
	
	@PreAuthorize("hasAnyRole(@environment.getRequiredProperty('" + "oauth.autorizacion.operaciones." + "ms-poc.products').split(','))")
	@PostMapping
	public ResponseEntity<Void> create(@Valid @RequestBody ProductDTO productDTO) {
		
		productService.create(productDTO);
		
		return ResponseEntity.status(HttpStatus.CREATED.value()).build();
	}
	
	
	@GetMapping
	public ResponseEntity<List<ProductDTO>> findAll () {
		List<ProductDTO> productos = productService.findAll();
		return ResponseEntity.ok(productos);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<ProductDTO> update(@PathVariable("id") Long id, @Valid @RequestBody ProductDTO productDTO) {
		productService.update(productDTO, id);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ProductDTO> findOne (@PathVariable("id") Long id) {
		ProductDTO product = productService.getOne(id);
		return ResponseEntity.ok(product);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
		productService.delete(id);
		return ResponseEntity.ok().build();
	}
}

package cl.poc.controller;

import cl.poc.dto.product.CategoriaDTO;
import cl.poc.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("categories")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class CategoryController {
	final CategoryService categoryService;
	@PostMapping()
	public ResponseEntity<Void> create(@RequestBody CategoriaDTO categoriaDTO) {
		
		categoryService.createCategory(categoriaDTO);
		
		return ResponseEntity.status(201).build();
	}
}
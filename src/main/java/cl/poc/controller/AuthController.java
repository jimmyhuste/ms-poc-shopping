package cl.poc.controller;

import cl.poc.dto.auth.SignInDTO;
import cl.poc.dto.auth.SignInReponseDTO;
import cl.poc.dto.auth.SignUpDTO;
import cl.poc.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("auth")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class AuthController {
	final AuthService authService;
	@PostMapping("/signup")
	public ResponseEntity<Void> signup(@RequestBody SignUpDTO signUpDTO) {
		
		authService.signup(signUpDTO);
		
		return ResponseEntity.status(201).build();
	}
	
	@PostMapping("/login")
	public ResponseEntity<SignInReponseDTO> login(@RequestBody SignInDTO signInDTO) {
		
		SignInReponseDTO signInReponseDTO = authService.login(signInDTO);
		
		return ResponseEntity.ok(signInReponseDTO);
	}
	@GetMapping("/users")
	public ResponseEntity<List<SignUpDTO>> findAll() {
		List<SignUpDTO> signUpDTOList = authService.findAll();
		return ResponseEntity.ok(signUpDTOList);
	}
}

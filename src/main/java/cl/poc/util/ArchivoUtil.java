package cl.poc.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.tika.detect.Detector;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AutoDetectParser;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.util.Base64;

@Slf4j
public class ArchivoUtil {
	
	private static final String IGUAL = "=";
	private static final long MIL = 1000;
	
	private ArchivoUtil() {
	
	}
	
	public static long calcularTamanoDeArchivoBase64EnKilobytes(final String archivoBase64) {
		long padding = 0;
		
		if(archivoBase64.endsWith(IGUAL + IGUAL)) {
			padding = 2;
		}
		else if(archivoBase64.endsWith(IGUAL)) {
			padding = 1;
		}
		
		long bytes = (archivoBase64.length() / 4) * 3 - padding;
		
		return bytes / MIL;
	}
	
	public static String obtenerExtensionDeArchivoBase64(final String archivoBase64) {
		try {
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(Base64.getDecoder().decode(archivoBase64));
			BufferedInputStream bufferedInputStream = new BufferedInputStream(byteArrayInputStream);
			
			AutoDetectParser autoDetectParser = new AutoDetectParser();
			Detector detector = autoDetectParser.getDetector();
			
			Metadata metadata = new Metadata();
			
			MediaType mediaType = detector.detect(bufferedInputStream, metadata);
			
			return mediaType.toString();
		}
		catch(Exception ex) {
			log.error("### [Error] Obteniedo la Extension del Archivo Base64", ex.getCause());
			return "";
		}
	}
}
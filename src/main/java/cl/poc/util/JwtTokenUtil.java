package cl.poc.util;

import cl.poc.config.JwtConfiguration;
import io.jsonwebtoken.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
@RequiredArgsConstructor
public class JwtTokenUtil {
	private final JwtConfiguration jwtConfiguration;
	
	private final String USERNAME = "username";
	private final String ROLES = "roles";
	private final String SCOPE = "scope";
	private final String RUT_CLIENTE = "rut_cliente";
	private final String APERTURA_CORCHETE = "[";
	private final String COMA = ",";
	private final String ESPACIO = " ";
	private final String CIERRE_CORCHETE = "]";
	private final int MIL = 1000;
	private final String EXP = "exp";
	
	public String generateToken(final String rutCliente, final String username, final List<String> roles) {
		return createToken(rutCliente, username, roles);
	}
	
	private String createToken(final String rutCliente, final String username, final List<String> roles) {
		final Map<String, Object> claims = new HashMap<>();
		claims.put(RUT_CLIENTE, rutCliente);
		claims.put(SCOPE, "read");
		claims.put(USERNAME, username);
		claims.put(ROLES, generateRoles(roles));
		
		return Jwts
				.builder()
				.setClaims(claims)
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + (jwtConfiguration.getDuration() * jwtConfiguration.getDuration()) * MIL))
				.signWith(SignatureAlgorithm.HS512, jwtConfiguration.getSecret())
				.compact();
	}
	
	private String generateRoles(final List<String> roles) {
		StringBuilder rolesFormateados = new StringBuilder(APERTURA_CORCHETE);
		for (int i = 0; i < roles.size(); i++) {
			if (i != (roles.size() - 1)) {
				rolesFormateados.append(roles.get(i)).append(COMA + ESPACIO);
			} else {
				rolesFormateados.append(roles.get(i)).append(CIERRE_CORCHETE);
			}
		}
		return roles.toString();
	}
	
	private Claims getAllClaimsFromToken(String token) {
		return Jwts
				.parserBuilder()
				.setSigningKey(jwtConfiguration.getSecret())
				.build()
				.parseClaimsJws(token)
				.getBody();
	}
	
	public String getClaimFromToken(String token, String key) {
		return getAllClaimsFromToken(token).get(key).toString();
	}
	
	public int getExpirationFromToken(String token) {
		return Integer.parseInt(getAllClaimsFromToken(token).get(EXP).toString());
	}
	
	public String getScopeFromToken(String token) {
		return getAllClaimsFromToken(token).get(SCOPE).toString();
	}
	public boolean isValidToken(String token) {
		try {
			Jwts
					.parserBuilder()
					.setSigningKey(jwtConfiguration.getSecret())
					.build()
					.parseClaimsJws(token)
					.getBody();
			return true;
		} catch (SignatureException | ExpiredJwtException ex) {
			return false;
		}
	}
	public boolean verifyToken(String token) {
		try {
			Claims claims = getAllClaimsFromToken(token);
			if (claims.getExpiration().before(new Date())) {
				return false; // Expired token
			}
			return true;
		} catch (SignatureException | MalformedJwtException | UnsupportedJwtException | IllegalArgumentException ex) {
			log.warn("Invalid JWT token: {}", ex.getMessage());
			return false;
		}
	}
	
}
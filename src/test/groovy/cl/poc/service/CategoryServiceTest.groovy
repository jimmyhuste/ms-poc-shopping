package cl.poc.service

import cl.poc.dto.product.CategoriaDTO
import cl.poc.model.product.Category
import cl.poc.repository.CategoryRepository
import spock.lang.Specification

class CategoryServiceTest extends Specification {

    CategoryRepository categoryRepository = Mock(CategoryRepository)
    CategoryService categoryService = new CategoryService(categoryRepository)

    def setup() {
        categoryService = new CategoryService(categoryRepository)
    }

    def "Crear Categoria de producto"() {
        given: "Una solicitud de crear categoria de producto"
        CategoriaDTO categoriaDTO = CategoriaDTO.builder()
                .nombre("Madera")
                .descripcion("Es una categoría de producto muy demandada en el mercado")
                .build()

        when: "Creamos el producto"
        categoryRepository.findByNombreContainingIgnoreCase(_ as String) >> Optional.empty()
        categoryRepository.save(_ as Category) >> {}
        categoryService.createCategory(categoriaDTO)

        then: "Validamos que se creó de manera ok"
        noExceptionThrown()
    }

    def "Validar obtener Categoria"() {
        given: "Una solicitud de obtener categoria"

        Category category = Category
                .builder()
                .nombre("Galletas")
                .descripcion("Es una galleta")
                .build()

        when: "Consultamos"
        categoryRepository.findByNombreContainingIgnoreCase(_ as String ) >> Optional.of(category)
        def response = categoryService.obtenerCategoria("Categoria")
        then: "Validamos que se encuente la Categoria ok"
        response != null
        response == category // Verifica que la respuesta sea igual a la categoría esperada

    }

    def "Validar no existencia de categoría"() {
        given: "Una solicitud de validar no existencia de categoría"

        String nombreNoExistente = "Galletas";

        when: "Validamos"
        categoryRepository.findByNombreContainingIgnoreCase(_ as String) >> Optional.empty();
        categoryService.validarExistenciaCategoria(nombreNoExistente);

        then: "Validamos que este vacio"
        noExceptionThrown()
    }


}


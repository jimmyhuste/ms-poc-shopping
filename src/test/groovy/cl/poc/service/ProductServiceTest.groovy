package cl.poc.service

import cl.poc.dto.archivo.FotografiaDTO
import cl.poc.dto.product.CategoriaDTO
import cl.poc.dto.product.ProductDTO
import cl.poc.model.product.Category
import cl.poc.model.product.Fotografia
import cl.poc.model.product.Product
import cl.poc.repository.FotografiaRepository
import cl.poc.repository.ProductRepository
import spock.lang.Specification

class ProductServiceTest extends Specification {

    // INJECTANDO REPOSITORIES
    ProductRepository productRepository = Stub(ProductRepository)
    FotografiaRepository fotografiaRepository = Stub(FotografiaRepository)
    CategoryService categoryService = Mock(CategoryService)

    ProductService productService
    String fotoBase64 = "UklGRuIEAABXRUJQVlA4WAoAAAA4AAAAHwAAHwAASUNDUKgBAAAAAAGobGNtcwIQAABtbnRyUkdCIFhZWiAH3AABABkAAwApADlhY3NwQVBQTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9tYAAQAAAADTLWxjbXMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlkZXNjAAAA8AAAAF9jcHJ0AAABTAAAAAx3dHB0AAABWAAAABRyWFlaAAABbAAAABRnWFlaAAABgAAAABRiWFlaAAABlAAAABRyVFJDAAABDAAAAEBnVFJDAAABDAAAAEBiVFJDAAABDAAAAEBkZXNjAAAAAAAAAAVjMmNpAAAAAAAAAAAAAAAAY3VydgAAAAAAAAAaAAAAywHJA2MFkghrC/YQPxVRGzQh8SmQMhg7kkYFUXdd7WtwegWJsZp8rGm/fdPD6TD//3RleHQAAAAAQ0MwAFhZWiAAAAAAAAD21gABAAAAANMtWFlaIAAAAAAAAG+iAAA49QAAA5BYWVogAAAAAAAAYpkAALeFAAAY2lhZWiAAAAAAAAAkoAAAD4QAALbPQUxQSOMAAAABgLNt2/FW/13b1mabO0fbdrtlqtvdmGzb1tY12arJtpO2H963RyNiAvCXDYMELT2tlVFWhPwGrsTMr1tpKkxSHGynxNxFwQwcY6/FvGtkOBWISQ7JcYgRk21ns74nJM5iWRKTvjVj8BeTr2eYpnCjC8DkgYI4DkC4mGYngCoqu1JAL5UjRWCaypkaMErlWAVooCKSAXKozAFw/aJRDEBGSOHLEQAEFLakfmmdkAsEYzKxEbD2EToyZFNZJ3LpBo4a0wQOPcFZpuyZz5gJ+Dr3PXLZCQNJ86Ix4enN5d5KtS/+UwBWUDggZgEAANAHAJ0BKiAAIAA+MRaJQ6IhIRQEACADBLYAWc5AH2AboBCCP63foB7KVGzPvZzQJofV0R0fIkBNsaCyjPtZO7i8GAoMMZAAAP7/2IU5KYaICuQuE673/zV6nm15ZZVVjFij/8GqXt23f9efTH6WHWl4a4hBJDWk4JTL43Mff/rnzr9+0o11PIBH4xVvQLqSz6ow/2wGM99ZNy8T+fnth9sFmTGZ/QUolSAFgx1FHyBXJ5g3eh0PJ4quUOH5Dt+wOf7C7NkmQZfm2eDxl63MqMEImI+En4UAFsjTI//Zqv/ugPr/82O6sntTv/BASvPH0/+bJcDfyW8qpL/+XOYXG/aaKuvitb3Y3C/VokpSEuvs7/NXiZdWcTOBehAAg7gb7Zz/gpf+f/P3vFWIhEp+D96XL4/boeTreWadMnSo+ZilYMZtQllLXTmAnjR4yqV65eUnf68j/WhEkQq0e//5OP6+320AAABFWElGugAAAEV4aWYAAElJKgAIAAAABgASAQMAAQAAAAEAAAAaAQUAAQAAAFYAAAAbAQUAAQAAAF4AAAAoAQMAAQAAAAIAAAATAgMAAQAAAAEAAABphwQAAQAAAGYAAAAAAAAASRkBAOgDAABJGQEA6AMAAAYAAJAHAAQAAAAwMjEwAZEHAAQAAAABAgMAAKAHAAQAAAAwMTAwAaADAAEAAAD//wAAAqAEAAEAAAAgAAAAA6AEAAEAAAAgAAAAAAAAAA=="

    def setup() {
        productService = new ProductService(productRepository, fotografiaRepository, categoryService)
    }

    def "Crear Producto"() {
        given: "una solicitud de creacion de producto"
        List<FotografiaDTO> fotografias = Arrays.asList(FotografiaDTO
                .builder()
                .nombre("Imagen")
                .extension(".jpg")
                .foto(fotoBase64)
                .build())
        ProductDTO productDTO = ProductDTO
                .builder()
                .nombre("Galleta")
                .categoria(CategoriaDTO
                        .builder()
                        .nombre("Gelltas")
                        .descripcion("Es una muy buena galleta")
                        .build())
                .descripcion("Galletas sabor de limón")
                .descuento("0")
                .fotografias(fotografias)
                .build()
        Category category = Category
                .builder()
                .nombre("Galletas")
                .descripcion("Es una galleta")
                .build()

        when: "Creamos el prodcuto"
        categoryService.obtenerCategoria(_ as String) >> category
        productRepository.save(_ as Product) >> {}
        fotografiaRepository.saveAll(_ as List<Fotografia>) >> {}
        productService.create(productDTO)

        then: "Validamos que el sea de manera correcta"
        noExceptionThrown()
    }

    def "Obtener Todo Productos"() {
        given: "Una solicitud de obtener un listado de productos"

        List<Fotografia> fotografia = Arrays.asList(Fotografia
                .builder()
                .id(1)
                .nombre("Foto 1 Nombre")
                .extension(".jpg")
                .foto(Base64.getDecoder().decode(fotoBase64))
                .build())

        Product product = Product.builder()
                .id(1)
                .nombre("Producto 1 Nombre")
                .precio(10.0)
                .descuento("0%")
                .descripcion("Producto 1 Descripción")
                .fotografias(fotografia)
                .category(Category
                        .builder()
                        .id(1)
                        .nombre("Categoría 1 Nombre")
                        .descripcion("Categoría 1 Descripción")
                        .build())
                .build()

        def productos = Arrays.asList(product, product)

        when: "Obtenemos los productos"
        productRepository.findAll() >> productos
        def response = productService.findAll()

        then: "validamos que lleguen todos los productos okey"
        response != null
        !response.isEmpty()
        response.size() == 2
    }

    def "Obtener un Producto"() {
        given: "Un solicictud de obtener un producto"

        def idProducto = 1

        List<Fotografia> fotografia = Arrays.asList(Fotografia
                .builder()
                .idProducto(1)
                .id(1)
                .nombre("Foto 1 Nombre")
                .extension(".jpg")
                .foto(Base64.getDecoder().decode(fotoBase64))
                .build())

        Product product = Product.builder()
                .id(1)
                .nombre("Producto 1 Nombre")
                .precio(10.0)
                .descuento("0%")
                .descripcion("Producto 1 Descripción")
                .fotografias(fotografia)
                .category(Category
                        .builder()
                        .id(1)
                        .nombre("Categoría 1 Nombre")
                        .descripcion("Categoría 1 Descripción")
                        .build())
                .build()

        when: "Obtenemos el producto"
        productRepository.findById(_ as Long) >> Optional.of(product)
        def response = productService.getOne(idProducto)

        then: "Validamos que se obtiene de forma correcta"
        response != null
    }

    def "Actualizar Producto"() {
        given: "una peticion para actualizar producto"
        def idProducto = 1

        List<FotografiaDTO> fotografias = Arrays.asList(FotografiaDTO
                .builder()
                .id(1)
                .nombre("Foto 1 Nombre")
                .extension(".jpg")
                .foto(fotoBase64)
                .build())

        Product product = Product.builder()
                .id(1)
                .nombre("Producto 1 Nombre")
                .precio(10.0)
                .descuento("0%")
                .descripcion("Producto 1 Descripción")
                .fotografias(fotografias as List<Fotografia>)
                .category(Category
                        .builder()
                        .id(1)
                        .nombre("Categoría 1 Nombre")
                        .descripcion("Categoría 1 Descripción")
                        .build())
                .build()
        def nombreCategoria = Category.builder().nombre("Es un nombre Categoria").build()

        ProductDTO productDTO = ProductDTO
                .builder()
                .nombre("Galleta")
                .categoria(CategoriaDTO
                        .builder()
                        .nombre("Gelltas")
                        .descripcion("Es una muy buena galleta")
                        .build())
                .descripcion("Galletas sabor de limón")
                .descuento("0")
                .fotografias(fotografias)
                .build()

        when: "Actualizamos el producto"
        productRepository.findById(_ as Long) >> Optional.of(product)
        categoryService.obtenerCategoria(_ as String) >> nombreCategoria
        productService.update(productDTO, idProducto)
        def response = productRepository.save(product)

        then: "Validamos que se ha actualizado correctamente"
        response != null
    }

    def "Eliminar Producto"() {
        given: "una soicitud de eliminacion de producto"
        def idProducto = 1

        List<Fotografia> fotografia = Arrays.asList(Fotografia
                .builder()
                .idProducto(1)
                .id(1)
                .nombre("Foto 1 Nombre")
                .extension(".jpg")
                .foto(Base64.getDecoder().decode(fotoBase64))
                .build())

        Product product = Product.builder()
                .id(1)
                .nombre("Producto 1 Nombre")
                .precio(10.0)
                .descuento("0%")
                .descripcion("Producto 1 Descripción")
                .fotografias(fotografia)
                .category(Category
                        .builder()
                        .id(1)
                        .nombre("Categoría 1 Nombre")
                        .descripcion("Categoría 1 Descripción")
                        .build())
                .build()

        when: "elminamos el producto"
        productRepository.findById(_ as Long) >> Optional.of(product)
        productService.delete(idProducto)

        then: "Validamos que se elmino correctamente"
        noExceptionThrown()

    }
}

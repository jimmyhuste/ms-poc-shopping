package cl.poc.controller

import cl.poc.dto.product.CategoriaDTO
import cl.poc.service.CategoryService
import com.google.gson.Gson
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup

class CategoryControllerTest extends Specification {
    CategoryService categoryService = Mock(CategoryService)
    CategoryController categoryController = new CategoryController(categoryService)

    def gson = new Gson()
    def mockMvc

    def setup() {
        mockMvc = standaloneSetup(categoryController).build()
    }


    def "Create Category"() {
        given: "Una solicitud de creacion de categoria"

        CategoriaDTO categoriaDTO = CategoriaDTO
                .builder()
                .nombre("Madera")
                .descripcion("Es una madera de buena calidad")
                .build()

        when: "creamos la categoria"
        def response = mockMvc.perform(post("/categories")
        .contentType(MediaType.APPLICATION_JSON)
        .content(gson.toJson(categoriaDTO)))
        .andReturn().response

        then: "Validamos que se creo de manera ok"
        response.status == HttpStatus.CREATED.value()
    }
}

package cl.poc.controller

import cl.poc.dto.archivo.FotografiaDTO
import cl.poc.dto.product.CategoriaDTO
import cl.poc.dto.product.ProductDTO
import cl.poc.service.ProductService
import com.google.gson.Gson
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import spock.lang.Specification

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup

class PorductControllerTest extends Specification {

    ProductService productService = Mock(ProductService)

    PorductController producContoller = new PorductController(productService)

    def gson = new Gson()
    def mockMvc
    String fotoBase64


    def setup() {
        mockMvc = standaloneSetup(producContoller).build()

        Path path = Paths.get("src/test/resources/foto64.txt");
        fotoBase64 = Files.readAllLines(path).get(0);
    }

    def "Crear Producto OK"() {
        given: "Una solicitud de creacion de producto"
        List<FotografiaDTO> fotografias = Arrays.asList(FotografiaDTO
                .builder()
                .nombre("Imagen")
                .extension(".jpg")
                .foto(fotoBase64)
                .build())
        ProductDTO productDTO = ProductDTO
                .builder()
                .nombre("Galleta")
                .categoria(CategoriaDTO
                        .builder()
                        .nombre("Gelltas")
                        .descripcion("Es una muy buena galleta")
                        .build())
                .descripcion("Galletas sabor de limón")
                .descuento("0")
                .precio(100)
                .fotografias(fotografias)
                .build()

        when: "Vamos a crear"
        def response = mockMvc.perform(post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(productDTO)))
                .andReturn().response

        then: "Validamos que sea manera correcta"
        response.status == HttpStatus.CREATED.value()
    }

    def "Altualizar Producto OK"() {
        given: "Una solicitud de actualizar producto"

        def productId = 1
        List<FotografiaDTO> fotografias = Arrays.asList(FotografiaDTO
                .builder()
                .id(1)
                .nombre("Imagen")
                .extension(".jpg")
                .foto(fotoBase64)
                .build())
        ProductDTO productDTO = ProductDTO
                .builder()
                .nombre("Galleta")
                .categoria(CategoriaDTO
                        .builder()
                        .id(1)
                        .nombre("Gelltas")
                        .descripcion("Es una muy buena galleta")
                        .build())
                .descripcion("Galletas sabor de limón")
                .descuento("0")
                .precio(100)
                .fotografias(fotografias)
                .build()

        when: "Vamos a actualizar el producto"
        def response = mockMvc.perform(put("/products/" + productId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(productDTO)))
                .andReturn().response

        then: "Validamos que sea manera correcta"
        response.status == HttpStatus.OK.value()
    }

    def "Obtener Producto OK"() {
        given: "Una solicitud de obtener producto"

        List<FotografiaDTO> fotografias = Arrays.asList(FotografiaDTO
                .builder()
                .id(1)
                .nombre("Imagen")
                .extension(".jpg")
                .foto(fotoBase64)
                .build())
        ProductDTO productDTO = ProductDTO
                .builder()
                .nombre("Galleta")
                .categoria(CategoriaDTO
                        .builder()
                        .id(1)
                        .nombre("Gelltas")
                        .descripcion("Es una muy buena galleta")
                        .build())
                .descripcion("Galletas sabor de limón")
                .descuento("0")
                .precio(100)
                .fotografias(fotografias)
                .build()

        when: "Vamos a obtener los productos"
        def response = mockMvc.perform(get("/products/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(productDTO)))
                .andReturn().response

        then: "Validamos que sea manera correcta"
        response.status == HttpStatus.OK.value()
    }

    def "Obtener Un Producto OK"() {
        given: "Una solicitud de obtener un solo producto"

        def productId = 1
        List<FotografiaDTO> fotografias = Arrays.asList(FotografiaDTO
                .builder()
                .id(1)
                .nombre("Imagen")
                .extension(".jpg")
                .foto(fotoBase64)
                .build())
        ProductDTO productDTO = ProductDTO
                .builder()
                .nombre("Galleta")
                .categoria(CategoriaDTO
                        .builder()
                        .id(1)
                        .nombre("Gelltas")
                        .descripcion("Es una muy buena galleta")
                        .build())
                .descripcion("Galletas sabor de limón")
                .descuento("0")
                .precio(100)
                .fotografias(fotografias)
                .build()

        when: "Vamos a obtener el producto"
        def response = mockMvc.perform(get("/products/" + productId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(productDTO)))
                .andReturn().response

        then: "Validamos que sea manera correcta"
        response.status == HttpStatus.OK.value()
    }

    def "Eliminar Un Producto OK"() {
        given: "Una solicitud de obtener un solo producto"

        def productId = 1
        List<FotografiaDTO> fotografias = Arrays.asList(FotografiaDTO
                .builder()
                .id(1)
                .nombre("Imagen")
                .extension(".jpg")
                .foto(fotoBase64)
                .build())
        ProductDTO productDTO = ProductDTO
                .builder()
                .nombre("Galleta")
                .categoria(CategoriaDTO
                        .builder()
                        .id(1)
                        .nombre("Gelltas")
                        .descripcion("Es una muy buena galleta")
                        .build())
                .descripcion("Galletas sabor de limón")
                .descuento("0")
                .precio(100)
                .fotografias(fotografias)
                .build()

        when: "Vamos a obtener el producto"
        def response = mockMvc.perform(delete("/products/" + productId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(productDTO)))
                .andReturn().response

        then: "Validamos que sea manera correcta"
        response.status == HttpStatus.OK.value()
    }
}